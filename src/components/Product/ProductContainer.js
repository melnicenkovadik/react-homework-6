import React, {Fragment, useState} from 'react';
import Modal from '../Modal/Modal';
import Product from './Product';
import Button from '../UI/Button/Button';


const ProductContainer = props => {
  const [product, setProduct] = useState({
    isFavorite: props.isFavorite,
    isInCart: props.isInCart,
  });

  const [isModal, setModal] = useState(false);


  const handleSetProduct = (id, local, target) => {
    const localProducts = JSON.parse(localStorage.getItem(local));
    console.log(localProducts);
    const currentProduct = props;

    if (!localProducts.some(product => product.id === id)) {
      localProducts.push(currentProduct)
    } else {
      const idx = localProducts.indexOf(localProducts.find(product => product.id === id));
      localProducts.splice(idx, 1);
    }

    setProduct({
      ...product,
      [target]: !product[target]
    });

    localStorage.setItem(local, JSON.stringify(localProducts))
  };

  const setProductToCart = (id) => {
    handleModalToggle();
    handleSetProduct.call(this, id, 'cart', 'isInCart')
  };

  const setProductToFavorites = (id) => {
    handleSetProduct.call(this, id, 'favorites', 'isFavorite');
  };

  const handleModalToggle = () => {
    setModal(!isModal);
  };

  const modal =
    <Modal
      header={'Подтверждение'}
      text={`Вы уверены, что хотите добавить товар ${props.name} в корзину?`}
      closeButton={true}
      closeHandler={handleModalToggle}
      type={'submit'}
      actions={[<Button
        key={1}
        text={'ОК'}
        onClick={() => {
          setProductToCart(props.id)
        }}
        backgroundColor={'red'}
      />]}
    />;

  return (
    <Fragment>
      <Product
        name={props.name}
        src={props.image}
        price={props.price}
        isFavorite={product.isFavorite}
        isInCart={product.isInCart}
        id={props.id}
        setToCart={handleModalToggle}
        setToFavorite={() => {
          setProductToFavorites(props.id)
        }}
      />
      {isModal ? modal : null}
    </Fragment>
  )
};

export default ProductContainer