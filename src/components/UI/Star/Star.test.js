import React from 'react';
import { shallow, render } from 'enzyme';
import Star from './Star';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


describe('Favorite star button', () => {
  it('FontAwesome Icon should change style on click', () => {
    const wrapper = shallow(<Star isFavorite={false} />)
    expect(wrapper.find(FontAwesomeIcon).prop('icon')[0]).toEqual('far')
    wrapper.setProps({ isFavorite: true })
    expect(wrapper.find(FontAwesomeIcon).prop('icon')[0]).toEqual('fas')
    wrapper.setProps({ isFavorite: false })
    expect(wrapper.find(FontAwesomeIcon).prop('icon')[0]).toEqual('far')
  })

  it('Shuld call callback on click', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(<Star onClick={mockCallback} />)
    const clickCount = 10
    wrapper.simulate('click')
    expect(mockCallback.mock.calls.length).toBe(1);
  })
})
