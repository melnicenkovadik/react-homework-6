import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';

describe('Button component', () => {
  it('Shold be button', () => {
    const wrapper = shallow(<Button />)
    expect(wrapper.find('button')).toBeDefined()
  })

  it('Should call callback function', () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(<Button onClick={mockCallback}/>)
    wrapper.simulate('click')
    expect(mockCallback.mock.calls.length).toBe(1);
  })

  it('Shold have background color as in prop', () => {
    const wrapper = shallow(<Button backgroundColor='red'/>)
    expect(wrapper.find('button').prop('style')).toHaveProperty('backgroundColor', 'red')
  })
})