import {
  GET_CART_PRODUCTS,
  SET_PRODUCT_COUNT,
  REMOVE_FROM_CART
} from '../actions/cartActions'

const initialState = {
  cartProducts: [],
  cartSum: 0,
}

export const cartReducer = (state = initialState, { type, payload }) => {

  switch (type) {

    case GET_CART_PRODUCTS:
      return {
        ...state,
        cartProducts: payload,
      }

    case SET_PRODUCT_COUNT:
      return {
        ...state,
        cartProducts: state.cartProducts.map(prod => {
          return prod.id === payload.id
            ? {
              ...prod,
              count: payload.count
            }
            : prod
        })
      }

    case REMOVE_FROM_CART : 
      return {
        ...state,
        cartProducts: state.cartProducts.filter(prod => prod.id !== payload)
      }
    
    default:
      return state
  }
}
